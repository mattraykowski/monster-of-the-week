function onDrop(x, y, draginfo)
    if draginfo.isType("shortcut") then
        local sPlaybook, sRecord = draginfo.getShortcutData()
        local nodeChar = getDatabaseNode()

        Debug.console("drag: ", onDrop, sPlaybook, sRecord)
        Debug.console("getDatabaseNode(): ", getDatabaseNode())

        if StringManager.contains({"pcplaybook"}, sPlaybook) then
            Debug.console("pcplaybook!")
            CharManagerMotw.addPlaybookDB(nodeChar, sPlaybook, sRecord)
            return true
        else
            Debug.console("not pcplaybook?!")
        end
    end
end
