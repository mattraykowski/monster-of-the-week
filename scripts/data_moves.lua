-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

function onInit()
    -- Monster of the Week Basic Moves
    basic_moves = {
        ["Act Under Pressure"] = { name="Act Under Pressure", ranking= "1", direction= "+" },
        ["Kick Some Ass"] = { name="Kick Some Ass", ranking= "4", direction= "+" },
        ["Help Out"] = { name="Help Out", ranking= "2", direction= "+" },
        ["Investigate A Mystery"] = { name="Investigate A Mystery", ranking= "3", direction= "+" },
        ["Manipulate Someone"] = { name="Manipulate Someone", ranking= "1", direction= "+" },
        ["Protect Someone"] = { name="Protect Someone", ranking= "4", direction= "+" },
        ["Read A Bad Situation"] = { name="Read A Bad Situation", ranking= "3", direction= "+" },
        ["Use Magic"] = { name="Use Magic", ranking= "5", direction= "+" },
        ["Big Magic"] = { name="Big Magic", ranking= "5", direction= "+" },
    }
end