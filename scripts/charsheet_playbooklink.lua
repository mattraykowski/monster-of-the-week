function onInit()
    onValueChanged();
end

function onValueChanged()
    if isEmpty() then
        setVisible(false);
    else
        setVisible(true);
    end
end