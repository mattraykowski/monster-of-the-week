-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

function onInit()
    registerMenuItem(Interface.getString("list_menu_createitem"), "insert", 5);
	update();
	-- Construct default skills
  getCurrentSkills();
  Debug.chat("** Initializing");
end

function onListChanged()
  update();
end
  
function update()

end

function getCurrentSkills()
    local entrymap = {};
    
      -- local aTechnicalSkills = getDefaultAcademicSkills(false);
    -- Calculate what moves are already on the sheet.  
    for _,w in pairs(getWindows()) do
        local sSkillName = w.name.getValue();
        entrymap[sSkillName] = w;
  	-- Debug.chat("Skill: ", w);
    --   local node = w.getDatabaseNode();
  	-- Debug.chat("Skill: ", DB.getValue(w.getDatabaseNode(), "name", ""));
    --   local node2 = w.name.getValue();
  	-- Debug.chat("Skill2: ", node2);
    end
    
    -- Look over the basic moves and make sure theyre on the sheet.
    for k,t in pairs(DataMoves.basic_moves) do
        local matches = entrymap[k];
        if not matches then
            local w = createWindow();
            local myNode = window.getDatabaseNode().getPath();
            local linkedRanking = "...rankings.ranking" .. t.ranking;

            if w then
                w.name.setValue(k);
                local trackersPath = w.getDatabaseNode().getPath();
                local myPCNode = window.getDatabaseNode().getPath();
                DB.setValue(trackersPath .. ".number_trackers", "number", 0);
                DB.setValue(trackersPath .. ".p1_hidden", "number", 0);
                DB.setValue(trackersPath .. ".p1_tooltip", "string", "Forward");
                DB.setValue(trackersPath .. ".p2_hidden", "number", 1);
                DB.setValue(trackersPath .. ".p2_tooltip", "string", "Parameter 2");
                DB.setValue(trackersPath .. ".p3_hidden", "number", 1);
                DB.setValue(trackersPath .. ".p3_tooltip", "string", "Parameter 3");
                DB.setValue(trackersPath .. ".clichatcommand", "string", "/pbta 2d6+(a)+0");
                DB.setValue(trackersPath .. ".parameter_formula", "string", "/pbta 2d6+(a)+(p1)");
                DB.setValue(trackersPath .. ".parameter_formula_enabled", "number", 1);
                DB.setValue(trackersPath .. ".refa", "string", linkedRanking .. " : (a) available");
                DB.setValue(trackersPath .. ".refa_path", "string", linkedRanking);
                DB.setValue(trackersPath .. ".rollstype", "string", "pbta");
            end
        end
    end

  end