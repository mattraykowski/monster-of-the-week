function onDoubleClick(x, y)
    if getValue() > 0 then
        local rActor = ActorManager.getActor("pc", window.getDatabaseNode())
        Debug.console("rActor: ")
        Debug.console(rActor)
        -- local nodeWin = window.getDatabaseNode();
        -- Debug.console("nodeWin: ");
        -- Debug.console(nodeWin);
        -- local sHeroType = nodeWin.getChild("pc_hero_points").getValue();
        -- Debug.console("sHeroType: ");
        -- Debug.console(sHeroType);
        local msg = {font = "msgfont", icon = "heropoints"}
        msg.text = rActor.sName .. " is using a luck point."
        Comm.deliverChatMessage(msg)
        setValue(getValue() - 1)
    end
    return true
end
