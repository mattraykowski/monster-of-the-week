function onInit()
	
	DesktopManager.showDockTitleText(true);
	DesktopManager.setDockTitleFont("sidebar");
	DesktopManager.setDockTitleFrame("", 25, 2, 25, 5);
	DesktopManager.setDockTitlePosition("top", 2, 14);
	DesktopManager.setStackIconSizeAndSpacing(47, 27, 3, 3, 4, 0);
	DesktopManager.setDockIconSizeAndSpacing(100, 28, 0, 0);
	DesktopManager.setLowerDockOffset(2, 0, 2, 3);

	-- Playbook Data Type
	LibraryData.setRecordTypeInfo("pcplaybook", {
		sDisplayText = "library_recordtype_label_pcplaybooks",
		aDataMap = { "playbook", "referencepcplaybook" },
		aDisplayIcon = { "button_pcplaybooks", "button_pcplaybooks_down" },
		fToggleIndex = toggleCharRecordIndex,
		sRecordDisplayClass = "pcplaybook",
	});
	
end

