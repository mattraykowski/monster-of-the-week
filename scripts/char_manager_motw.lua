-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--
CharManagerMotw = {addPlaybookDB = addPlaybookDB}

--
-- CHARACTER SHEET DROPS
--

function addPlaybookDB(nodeChar, sPlaybook, sRecord)
    --	Debug.console("char_manager_dbd.lua","addPlaybookDB","nodeChar1",nodeChar);
    Debug.console("char_manager_dbd.lua", "addPlaybookDB", "sRecord", sRecord)
    --	Debug.console("char_manager_dbd.lua","addPlaybookDB","sPlaybook",sPlaybook);
    --	Debug.console("DB: ", DB.getValue(nodeChar, "playbooklink", ""));
    --	Debug.console("nodeChar: ", nodeChar);

    DB.setValue(nodeChar, "playbooklink", "windowreference", sPlaybook, sRecord)
    --		Debug.console("setLink!", DB.getValue(nodeChar, "playbooklink.recordname"));
    --		Debug.console("setClass!", DB.getValue(sRecord .. ".name"));
    nodeChar.getChild("playbook").setValue(DB.getValue(sRecord .. ".name"))
    -- local nCurLevel = tonumber(DB.getValue(nodeChar, "pclevel", "number"));
    -- nodeChar.getChild("pclevel").setValue(nCurLevel + 1);

    -- CharManagerDBD.addClassLevel(nodeChar, sPlaybook, sRecord);
end

function setHarm(nodeChar, harmLevel, setting)
    setting = setting or 1
    local wounds = nodeChar.getChild("wounds")
    local unstable = nodeChar.getChild("unstable")
    local newWounds = harmLevel

    -- Update the harm checkboxes.
    -- Clicking a box in the middle will unset the box but  we want 
    -- actually want to choose it. So we need to set it to 0 so that
    -- the click actually ends up setting it to 1. The same goes for 
    -- the situation where we click something that is already selected,
    -- we want to update wounds and uncheck it.
    for h = 1, 7, 1 do
        local harmBox = nodeChar.getChild("harm_" .. h)
        if (h < harmLevel) then
            harmBox.setValue(1)
        else
            if (h > harmLevel) then
                harmBox.setValue(0)
            else
                if (h == harmLevel and harmLevel == wounds.getValue()) then
                    newWounds = newWounds - 1
                    harmBox.setValue(1)
                else
                    if (h == harmLevel and harmLevel < wounds.getValue()) then
                        harmBox.setValue(0)
                    end
                end
            end
        end
    end

    -- Update the unstable checkbox.
    if newWounds > 3 and wounds.getValue() < newWounds then
        -- If the new harm is greater than the unstable mark and has increased
        -- since the last type harm was set we will mark it unstable.
        unstable.setValue(1)
    else
        -- Check to see if we're currently unstable and are back in stable
        -- territory. If we are, uncheck the unstable box.
        if newWounds <= 3 and unstable.getValue() == 1 then
            unstable.setValue(0)
        end
    end

    -- Set the current wounds to the harm level.
    nodeChar.getChild("wounds").setValue(newWounds)

end
